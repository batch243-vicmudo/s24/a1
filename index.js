//console.log("Hello World");
const number = 2
const getCube = Math.pow(number, 3);

console.log(`The cube of ${number} is ${getCube}`);

const address = ['258 Washington Ave NW', 'California', '90011'];

const [city, country, localcode] = address;
console.log(`I live at ${city}, ${country} ${localcode}`);

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighted at ${weight} with a measurement of ${measurement}.`);

const num = [1,2,3,4,5];

num.forEach((number)=>{
	console.log(number);
})
const reduceNumber = num.reduce((a, v) =>{
	return a + v;
}, 0);

console.log(reduceNumber);

class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
	}
}

let dog = new Dog("Frankie", "5", "Miniature Dachshund");
console.log(dog);